# Choose Your Own Salt-venture!

## Why?

It's no secret that the Salt documentation has some weak spots. This project
was created as a quick start to actually produce some sweet sweet documentation
for Salt. The existing documentation has some really good things and we'll
absolutely take from that, but the ultimate goal is that this gets either
merged into Salt or into some other official Salt repository.

In the meantime... if you're interested, join the discussion at https://gitlab.com/waynew/choose-your-own-saltventure

## How?

[Choose Your Own
Adventure](https://en.wikipedia.org/wiki/Choose_Your_Own_Adventure) books were
awesome. We are going to write *this* set of documentation following a bit of a
similar style... only with less dead-ends.

## Okay You've Convinced Me, Now What?

Sweet action! The first thing we need is a list of different scenarios that
might have you coming to the Salt project. So... here's the list we have so far
-- if you have any more ideas, just open a PR at
https://gitlab.com/waynew/choose-your-own-saltventure .
Also feel free to open up an issue if there's something that you'd like to
discuss!


## Entry Points

- [I'm a newbie to automation, help!][newbie]
- [I just want to get Salt managing my systems][salt-install]
- [I think the documentation could be improved][doc-contribute]
- [I know Python and would like to contribute code to Salt][code-contribute]
- [I have a problem and need some help][help-me]
- [I would like to help others use Salt better][help-others]
- [I believe I found a bug in Salt][bug-report]
- [I wish Salt would do something new!][feature-request]
- [Shut up and take my money!][enterprise]



[newbie]: ???
[salt-install]: ???
[doc-contribute]: ???
[code-contribute]: ???
[help-me]: ???
[help-others]: ???
[bug-report]: ???
[feature-request]: ???
[enterprise]: ???
